<?php
	
    $contentType           = isset($contentType)? $contentType :"";
    $contentTypeTitle      = isset($contentTypeTitle)? $contentTypeTitle :"";
    $contentTypeLink       = isset($contentTypeLink)? $contentTypeLink :"";
	$contentTypeNo     	   = isset($contentTypeNo)? $contentTypeNo : 1;


    $chartList = array(
        array(
            'no' => 1,
            'active' => '',
            'filter' => '筛选1'
        ),
        array(
            'no' => 2,
            'active' => 'active',
            'filter' => '筛选2'
        ),
        array(
            'no' => 3,
            'active' => '',
            'filter' => '筛选3'
        ),
        array(
            'no' => 4,
            'active' => '',
            'filter' => '筛选4'
        )
    );
?>





<div class="section charts <?php print $contentType; ?>">

    <div class="chartLeft">
        <div class="chartTitle"><a href="<?php print $contentTypeLink; ?>"><?php print $contentTypeTitle; ?></a></div>
        <div class="chartItems">
            <?php

                foreach($chartList AS $list){

                    $chartNo        = $list['no'];
                    $chartActive    = $list['active'];
                    
                    include('inc/chart_items.php');

                }

            ?>
        </div>
    </div>
    <div class="chartRight">
        <div class="chartFilters">
            <ul>
            <?php
                foreach($chartList AS $list){
                    $filterId       = "type".$contentTypeNo."-filt".$list['no'];
                    $filterTitle    = $list['filter'];
            ?>
                <li class="cFilter <?php print $list['active']; ?>" id="<?php print $filterId; ?>">
                    <span class="cFilterTopBorder"></span>
                    <span class="cFilterTitle"><?php print $filterTitle; ?></span>
                </li>
            <?php
                }
            ?>
            </ul>
        </div>
        <?php

            foreach($chartList AS $list){

                $chartNo        = $list['no'];
                $chartActive    = $list['active'];
                
                include('inc/chart_displays.php');

            }

        ?>
    </div>

</div>