<?php

    $content_type           = isset($content_type)? $content_type :"";
    $content_type_title     = isset($content_type_title)? $content_type_title :"";


?>


<div id="main" class="fullWidth <?php print $content_type; ?>">
  <div class="pageCenter">
    <div class="header_small_trigger"></div>


        <div class="section banner">
            <span class="bannerTopBorder"></span>
            <img src="img/content/slide1.jpg">
        </div>


        <div class="section mainContent">

            <div class="mainHeader">
                <div class="mainTitle"><h1>陈奕迅2014年LIFE演唱会 成都站</h1></div>
                <div class="mainRating">
                    <div class="rStarGroup">
                        <span class="rStar rStarFull"></span>
                        <span class="rStar rStarFull"></span>
                        <span class="rStar rStarFull"></span>
                        <span class="rStar rStarHalf"></span>
                        <span class="rStar rStarEmpty"></span>
                        <input type="hidden" name="rating" value="3.5" >
                    </div>
                    <div class="rStarLabel">我的评分</div>
                </div>
            </div>

            <div class="mainBody">

                <div class="mLeftCol">
                    <div class="mColTitle">统计</div>
                    <div class="mColRows">
                        <ul>
                            <li class="mColRow rating first">
                                <?php include('inc/rating.php'); ?>
                                <div class="mColLabel">1200个评分</div>
                            </li>
                            <li class="mColRow comment">
                                <div class="mColIcon"></div>
                                <div class="mColLabel">199人评论</div>
                            </li>
                            <li class="mColRow gift last">
                                <div class="mColIcon"></div>
                                <div class="mColLabel">58个礼物</div>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <div class="mCenterCol">
                    <div class="mCntColTop">
                        <div class="mCntTopLabel">信息</div>
                        <div class="mCntTopIntro">角紧了展，维美工也开再性，内局张来就开也华作院手要到直利外角间发，对保水教脚价小：注城学深方中青让香表点车人清去出呢一活三是陆说有委选提会那化代转她子区院是？花使向任立，物花动中大人文听。分元一导中下这。内资态我须往所取和成会超的。</div>
                        <div class="mCntTopButton">
                            <span class="mCntBtnTitle">现在预订</span>
                            <span class="mCntBtnIcon"></span>
                        </div>
                    </div>
                    <div class="mCentColDesc">为销星衣什有下率排经举以女登读政影定饭乐的城它，再是放动看深利这，我那日电观，意始终可的，相提四了自年说三。我大会物纪自回无主能两地理；小式读出军营动小全大业家。没利上喜，推学戏为他光北，可是发社可保。致住化做多！月前饭目法……入可空希动重程物服；他社雄和车观是特神持当。民力子两生……手我兴。球报建就片作课学人主他想花，大本军变静一健得说考生；出下气，其后班投类交静当天两风上认时，手孩条价果中力然的，间王人风提生了把……一动天天华一保三？景多表文年，远以验股？义业麽出却这提！民东座样举。<br>
                    场重中日光吸此爸当我技电由他计、前动水心轮草为问朋却钱清力家许统他久球都要票故的：选多友看标对汽爱言这，来果向设的？主士企规制价它脑题供国全量令具算校竟人单。<br>
                    长天生是爸！<br>
                    民的推、我工如市希写现巴看消只。然不定因，人不说影上足家，府哥作？<br>
                    的成温，北断子发，真我外美事看种血我我张得不系……经热代，长下法听，得际地年完才是功安我学学曾是。常公地。事是食还速一现人于意妈举他，一麽客了要一把积把另上德而金山上告识的、我大为自。阳化成酒国医者不得天知有写不果人？<br>
                    假了造太原统约的球直真不全最济客老象友角表否石一集备不。</div>
                </div>
                
                <div class="mRightCol">
                    <div class="mColTitle">相关<?php print $content_type_title; ?></div>
                    <div class="mColRows">
                        <ul>
                            <li class="mColRow first">
                                <div class="mRefItem">
                                    <div class="mRefCover">
                                        <a href=""><img src="img/content/ref.jpg"></a>
                                    </div>
                                    <div class="mRefInfo">
                                        <div class="mRefTitle"><a href="">孙燕姿演唱会</a></div>
                                        <div class="mRefRating">
                                            <?php include('inc/rating.php'); ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="mColRow">
                                <div class="mRefItem">
                                    <div class="mRefCover">
                                        <a href=""><img src="img/content/ref.jpg"></a>
                                    </div>
                                    <div class="mRefInfo">
                                        <div class="mRefTitle"><a href="">孙燕姿演唱会</a></div>
                                        <div class="mRefRating">
                                            <?php include('inc/rating.php'); ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="mColRow last">
                                <div class="mRefItem">
                                    <div class="mRefCover">
                                        <a href=""><img src="img/content/ref.jpg"></a>
                                    </div>
                                    <div class="mRefInfo">
                                        <div class="mRefTitle"><a href="">孙燕姿演唱会</a></div>
                                        <div class="mRefRating">
                                            <?php include('inc/rating.php'); ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>


            </div>

        </div>



        <div class="section mainTabs">


    		<div class="mTabArea">
                <div class="mTab active" id="mTab-1">评论</div>
                <div class="mTab" id="mTab-2">礼物</div>
                <div class="mTab" id="mTab-3">照片</div>
                <div class="mTab" id="mTab-4">视频</div>
                <div class="mTab" id="mTab-5">新闻&活动</div>
                <div class="mTab" id="mTab-6">地点</div>
                <div class="mTab" id="mTab-7">日程</div>
    		</div>


            <div class="mCardArea">

              <div class="mCard comment active" id="mCard-1">
                <form action="" method="post" class="mCCmtForm">
                  <textarea class="inputTextarea placeholder">你怎么看？</textarea>
                  <div class="mCmtSubmit">
                    <input type="submit" value="发表">
                  </div>
                  <div class="mCmtFollow">
                    <input type="checkbox" id="followThis" checked="checked">
                    <label for="followThis">关注评论</label>
                  </div>
                </form>
                <div class="mCCmtSummary">138条评论</div>
                <div class="mCCmtHistory">
                  <div class="mCCmtItem">
                    <div class="mCCmtAccount">
                      <div class="mCCmtAvatar"><img src="img/content/avatar.jpg"></div>
                      <div class="mCCmtUsername">dotGreg</div>
                    </div>
                    <div class="mCCmtContent">
                      <div class="mCCmtText">角紧了展，维美工也开再性，内局张来就开也华作院手要到直利外角间发，对保水教脚价小：注城学深方中青让香表点车人清去出呢一活三是陆说有委选提会那化代转她子区院是？花使向任立，物花动中大人文听。分元一导中下这。内资态我须往所取和成会超的。</div>
                      <div class="mCCmtNo"># 1</div>
                      <div class="mCCmtDate">2014-01-30</div>
                      <div class="mCCmtReply"></div>

                      <div class="mCCmtReplyBox">
			<form action="" method="post" class="mCCmtForm">
                          <textarea class="inputTextarea placeholder">你怎么看？</textarea>
                          <div class="mCmtSubmit">
                            <input type="submit" value="发表">
                          </div>
			</form>
		      </div>


                    </div>
                  </div>
                  <div class="mCCmtItem">
                    <div class="mCCmtAccount">
                      <div class="mCCmtAvatar"><img src="img/content/avatar.jpg"></div>
                      <div class="mCCmtUsername">dotGreg</div>
                    </div>
                    <div class="mCCmtContent">
                      <div class="mCCmtText">角紧了展，维美工也开再性，内局张来就开也华作院手要到直利外角间发。<br>对保水教脚价小：注城学深方中青让香表点车人清去出呢一活三是陆说有委选提会那化代转她子区院是？花使向任立，物花动中大人文听。分元一导中下这。内资态我须往所取和成会超的。</div>
                      <div class="mCCmtNo"># 1</div>
                      <div class="mCCmtDate">2014-01-30</div>
                      <div class="mCCmtReply"></div>
		      <div class="mCCmtReplyBox">
			<form action="" method="post" class="mCCmtForm">
                          <textarea class="inputTextarea placeholder">你怎么看？</textarea>
                          <div class="mCmtSubmit">
                            <input type="submit" value="发表">
                          </div>
			</form>
		      </div>


                    </div>
                  </div>
                </div>
              </div>

                <div class="mCard gift" id="mCard-2">

                    <div class="mCGiftDisplay">
                        <div class="mCGiftUsers">
                            <img class="mCGiftUser" src="img/content/avatar_icon.jpg">
                            <img class="mCGiftUser" src="img/content/avatar_icon.jpg">
                            <img class="mCGiftUser" src="img/content/avatar_icon.jpg">
                            <img class="mCGiftUser" src="img/content/avatar_icon.jpg">
                            <img class="mCGiftUser" src="img/content/avatar_icon.jpg">
                            <img class="mCGiftUser" src="img/content/avatar_icon.jpg">
                            <img class="mCGiftUser" src="img/content/avatar_icon.jpg">
                            <img class="mCGiftUser" src="img/content/avatar_icon.jpg">
                        </div>
                        <div class="mCGiftSend">
                            <div class="mCGiftSubmit">送礼物</div>
                        </div>
                    </div>

                    <div class="mCGiftSummary">138条评论</div>
                    <div class="mCGiftHistory">
                        <div class="mCGiftItem">
                            <div class="mCGiftAccount">
                                <div class="mCGiftAvatar"><img src="img/content/avatar.jpg"></div>
                                <div class="mCGiftUsername">dotGreg</div>
                            </div>
                            <div class="mCGiftContent">
                                <div class="mCGiftText">角紧了展，维美工也开再性，内局张来就开也华作院手要到直利外角间发，对保水教脚价小：注城学深方中青让香表点车人清去出呢一活三是陆说有委选提会那化代转她子区院是？花使向任立，物花动中大人文听。分元一导中下这。内资态我须往所取和成会超的。</div>
                                <div class="mCGiftNo"># 1</div>
                                <div class="mCGiftDate">2014-01-30</div>
                            </div>
                            <div class="mCGiftToken"><img src="img/content/token.jpg"></div>
                        </div>
                        <div class="mCGiftItem">
                            <div class="mCGiftAccount">
                                <div class="mCGiftAvatar"><img src="img/content/avatar.jpg"></div>
                                <div class="mCGiftUsername">dotGreg</div>
                            </div>
                            <div class="mCGiftContent">
                                <div class="mCGiftText">角紧了展，维美工也开再性，内局张来就开也华作院手要到直利外角间发，对保水教脚价小：注城学深方中青让香表点车人清去出呢一活三是陆说有委选提会那化代转她子区院是？花使向任立，物花动中大人文听。分元一导中下这。内资态我须往所取和成会超的。</div>
                                <div class="mCGiftNo"># 1</div>
                                <div class="mCGiftDate">2014-01-30</div>
                            </div>
                            <div class="mCGiftToken"><img src="img/content/token.jpg"></div>
                        </div>
                        <div class="mCGiftItem">
                            <div class="mCGiftAccount">
                                <div class="mCGiftAvatar"><img src="img/content/avatar.jpg"></div>
                                <div class="mCGiftUsername">dotGreg</div>
                            </div>
                            <div class="mCGiftContent">
                                <div class="mCGiftText">角紧了展，维美工也开再性，内局张来就开也华作院手要到直利外角间发，对保水教脚价小：注城学深方中青让香表点车人清去出呢一活三是陆说有委选提会那化代转她子区院是？花使向任立，物花动中大人文听。分元一导中下这。内资态我须往所取和成会超的。</div>
                                <div class="mCGiftNo"># 1</div>
                                <div class="mCGiftDate">2014-01-30</div>
                            </div>
                            <div class="mCGiftToken"><img src="img/content/token.jpg"></div>
                        </div>
                    </div>

                </div>

                <div class="mCard album" id="mCard-3">

                    <div class="mCAlbum" id="album1">
                        <div class="mCAlbumTitle">专辑一</div>
                        <div class="mCAlbumPhotos">
                            <a class="mCAlbumPhoto mCAlbumPhoto-1" href="img/content/slide2.jpg" title="a"><img id="album1-p1" src="img/content/album.jpg"></a>
                            <a class="mCAlbumPhoto mCAlbumPhoto-1" href="img/content/slide1.jpg" title="a"><img id="album1-p2" src="img/content/album.jpg"></a>
                            <a class="mCAlbumPhoto mCAlbumPhoto-1" href="img/content/slide1.jpg" title="a"><img id="album1-p3" src="img/content/album.jpg"></a>
                            <a class="mCAlbumPhoto mCAlbumPhoto-1" href="img/content/slide1.jpg" title="a"><img id="album1-p4" src="img/content/album.jpg"></a>
                            <a class="mCAlbumPhoto mCAlbumPhoto-1" href="img/content/slide1.jpg" title="a"><img id="album1-p5" src="img/content/album.jpg"></a>
                            <a class="mCAlbumPhoto mCAlbumPhoto-1" href="img/content/slide1.jpg" title="a"><img id="album1-p6" src="img/content/album.jpg"></a>
                            <a class="mCAlbumPhoto mCAlbumPhoto-1" href="img/content/slide1.jpg" title="a"><img id="album1-p7" src="img/content/album.jpg"></a>
                        </div>
                    </div>

                    <div class="mCAlbum" id="album2">
                        <div class="mCAlbumTitle">专辑二</div>
                        <div class="mCAlbumPhotos">
                            <a class="mCAlbumPhoto mCAlbumPhoto-2" href="img/content/slide2.jpg"><img id="album2-p1" src="img/content/album.jpg"></a>
                            <a class="mCAlbumPhoto mCAlbumPhoto-2" href="img/content/slide2.jpg"><img id="album2-p2" src="img/content/album.jpg"></a>
                            <a class="mCAlbumPhoto mCAlbumPhoto-2" href="img/content/slide2.jpg"><img id="album2-p3" src="img/content/album.jpg"></a>
                            <a class="mCAlbumPhoto mCAlbumPhoto-2" href="img/content/slide2.jpg"><img id="album2-p4" src="img/content/album.jpg"></a>
                            <a class="mCAlbumPhoto mCAlbumPhoto-2" href="img/content/slide2.jpg"><img id="album2-p5" src="img/content/album.jpg"></a>
                            <a class="mCAlbumPhoto mCAlbumPhoto-2" href="img/content/slide2.jpg"><img id="album2-p6" src="img/content/album.jpg"></a>
                            <a class="mCAlbumPhoto mCAlbumPhoto-2" href="img/content/slide1.jpg"><img id="album2-p7" src="img/content/album.jpg"></a>
                        </div>
                    </div>

                </div>

                <div class="mCard video" id="mCard-4">

                    <div class="mCVideo" id="video1">
                        <div class="mCVideoTitle">视频一</div>
                        <div class="mCVideoCode">
                            <iframe height=360 width=510 src="http://player.youku.com/embed/XNTcyMjg0NjEy" frameborder=0 allowfullscreen></iframe>
                        </div>
                    </div>

                    <div class="mCVideo" id="video2">
                        <div class="mCVideoTitle">视频一</div>
                        <div class="mCVideoCode">
                            <iframe height=360 width=510 src="http://player.youku.com/embed/XNTcyMjg0NjEy" frameborder=0 allowfullscreen></iframe>
                        </div>
                    </div>

                </div>

                <div class="mCard news" id="mCard-5">

                    <div class="mCNews" id="news1">
                        <div class="mCNewsCover">
                            <img src="img/content/news.jpg">
                        </div>
                        <div class="mCNewsContent">
                            <div class="mCNewsTitle">美国原版芝麻街音乐剧Elmo</div>
                            <div class="mCNewsDate">2014-01-30</div>
                            <div class="mCNewsSummary">角紧了展，维美工也开再性，内局张来就开也华作院手要到直利外角间发，对保水教脚价小：注城学深方中青让香表点车人清去出呢一活三是陆说有委选提会那化代转她子区院是？花使向任立，物花动中大人文听。分元一导中下这。内资态我须往所取和成会超的。</div>
                        </div>
                    </div>

                    <div class="mCNews" id="news2">
                        <div class="mCNewsCover">
                            <img src="img/content/news.jpg">
                        </div>
                        <div class="mCNewsContent">
                            <div class="mCNewsTitle">美国原版芝麻街音乐剧Elmo</div>
                            <div class="mCNewsDate">2014-01-30</div>
                            <div class="mCNewsSummary">角紧了展，维美工也开再性，内局张来就开也华作院手要到直利外角间发，对保水教脚价小：注城学深方中青让香表点车人清去出呢一活三是陆说有委选提会那化代转她子区院是？花使向任立，物花动中大人文听。分元一导中下这。内资态我须往所取和成会超的。</div>
                        </div>
                    </div>

                </div>

                <div class="mCard map" id="mCard-6">

                    <div class="mCMapTop">
                        <div class="mCMapColumn">
                            <div class="mCMapTitle">地址</div>
                            <div class="mCMapDesc">角紧了展，维美工也开再性，<br>内局张来就开也华作院手要到直利外角间发，<br>对保水教脚价小：<br>注城学深方中青让香表点车人清去出呢一活三是<br>陆说有委选提会那化代转她子区院是？<br>花使向任立，物花动中大人文听。</div>
                        </div>
                        <div class="mCMapColumn">
                            <div class="mCMapTitle">地址</div>
                            <div class="mCMapDesc">角紧了展，维美工也开再性，<br>内局张来就开也华作院手要到直利外角间发，<br>对保水教脚价小：<br>注城学深方中青让香表点车人清去出呢一活三是<br>陆说有委选提会那化代转她子区院是？<br>花使向任立，物花动中大人文听。</div>
                        </div>
                        <div class="mCMapColumn">
                            <div class="mCMapTitle">地址</div>
                            <div class="mCMapDesc">角紧了展，维美工也开再性，<br>内局张来就开也华作院手要到直利外角间发，<br>对保水教脚价小：<br>注城学深方中青让香表点车人清去出呢一活三是<br>陆说有委选提会那化代转她子区院是？<br>花使向任立，物花动中大人文听。</div>
                        </div>
                    </div>

                    <div class="mCMapBottom">
                        <div class="mCMapEmbed">
                            <iframe width="680" height="460" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://ditu.google.cn/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=%E4%B8%8A%E6%B5%B7&amp;aq=&amp;sll=36.650997,117.120497&amp;sspn=0.776744,1.454315&amp;brcurrent=3,0x35b270634e678b93:0x9bab911934f9a6c2,0%3B5,0,0&amp;ie=UTF8&amp;hq=&amp;hnear=Shanghai&amp;t=m&amp;ll=31.230417,121.473684&amp;spn=0.033761,0.058451&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
                        </div>
                    </div>

                </div>

                <div class="mCard schedule" id="mCard-7">
                    <table class="mCScheForm">
                        <tr class="formHeader">
                            <th class="scheduleDate">日期</th>
                            <th class="scheduleVenue">场地</th>
                            <th class="scheduleTitle">名称</th>
                            <th class="scheduleBook">预定</th>
                        </tr>
                        <tr class="formRowOdd">
                            <td class="scheduleDate">2014-02-18</td>
                            <td class="scheduleVenue">上海大剧院</td>
                            <td class="scheduleTitle">芝麻街</td>
                            <td class="scheduleBook">
                                <div class="scheduleButton">
                                    <span class="buttonCart"></span>
                                </div>
                            </td>
                        </tr>
                        <tr class="formRowEven">
                            <td class="scheduleDate">2014-02-18</td>
                            <td class="scheduleVenue">上海大剧院</td>
                            <td class="scheduleTitle">芝麻街</td>
                            <td class="scheduleBook">
                                <div class="scheduleButton soldout">
                                    <span class="buttonCart"></span>
                                </div>
                            </td>
                        </tr>
                        <tr class="formRowOdd">
                            <td class="scheduleDate">2014-02-18</td>
                            <td class="scheduleVenue">上海大剧院</td>
                            <td class="scheduleTitle">芝麻街</td>
                            <td class="scheduleBook">
                                <div class="scheduleButton">
                                    <span class="buttonCart"></span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>


        </div>




  </div>
</div>
