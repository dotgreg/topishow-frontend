<?php
    
    $chartNo           = isset($chartNo)? $chartNo : 1;
    $chartActive       = isset($chartActive)? $chartActive : "";
    $contentType       = isset($contentType)? $contentType : "";
    $contentTypeNo     = isset($contentTypeNo)? $contentTypeNo : 1;
    $chartId           = "type".$contentTypeNo."-menu".$chartNo;


?>



<ul class="<?php print $chartActive; ?>" id="<?php print $chartId; ?>">

    <?php 
        for($i=1;$i<=10;$i++){ 
            $image_name = $i < 10 ? "S01E0".$i : "S01E".$i;
    ?>

    <li class="chartItem <?php 
    			print $i == 1 ? " first" : "";
    			print $i == 10 ? " last" : "";
    		?>" id="<?php
    			print 'type'.$contentTypeNo.'-menu'.$chartNo.'-'.$i; 
    		?>">
        <div class="cItemNumber"><?php print $i; ?></div>
        <div class="cItemTitle">陈奕迅<?php print $i == 1 ? $chartNo : ""; ?></div>
        <div class="cItemDetail">
            <div class="cItemRating"><?php include('inc/rating.php'); ?></div>
            <div class="cItemDesc">角紧了展，维美工也开再性，内局张来就开也华作院手要到直利外角间。</div>
        </div>
    </li>

    <?php } ?>

</ul>