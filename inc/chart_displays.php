<?php
    
    $chartNo           = isset($chartNo)? $chartNo : 1;
    $chartActive       = isset($chartActive)? $chartActive : "";
    $contentType       = isset($contentType)? $contentType : "";
    $contentTypeNo     = isset($contentTypeNo)? $contentTypeNo : 1;
    $chartId           = "type".$contentTypeNo."-disp".$chartNo;


?>

<div class="chartDisplays <?php print $chartActive; ?>" id="<?php print $chartId; ?>">

    <?php 
        for($i=1;$i<=10;$i++){ 
            $image_name = $i < 10 ? "S01E0".$i : "S01E".$i;
    ?>

    <div class="chartDisplay chartDisplay-<?php 
            print $i;
            print $i == 1 ? " first" : "";
            print $i == 10 ? " last" : "";
        ?>" id="<?php 
            print 'type'.$contentTypeNo.'-disp'.$chartNo.'-'.$i; 
        ?>">
        <span class="cDispTopBorder"></span>
        <img class="cDispImage" src="img/content/<?php print $image_name; ?>.jpg">
    </div>

    <?php } ?>

</div>