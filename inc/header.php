<!DOCTYPE html>
<html>
  <head>

    <title>Topishow - Homepage</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link rel="stylesheet" href="inc/colorbox/colorbox.css">
    <link rel="stylesheet" href="css/style.css">
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="css/ie8_7.css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="css/ie8.css" />
    <![endif]-->

    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="inc/colorbox/jquery.colorbox-min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/script.js"></script>

  </head>
  <body>

    <div id="header" class="fullWidth">
        <div id="headerTop" class="fullWidth">
            <div class="pageCenter">
                <ul id="topMenu">
                    <li><a href="">链接</a></li>
                    <li><a href="">链接</a></li>
                    <li><a href="">链接</a></li>
                    <li><a href="">链接</a></li>
                </ul>
            </div>
        </div>
        <div id="headerMain" class="fullWidth">
            <div id="categories" class="">
                <div class="categoryHover">
                    <div class="categoryIcon"></div>
                    <div class="categoryTitle">全部活动分类</div>
                </div>
                <div class="categoryDropdown">
                    <div class="catDropMain">
                        <ul>
                            <li class="catDropTab all active">
                                <span class="catDropTabTopBorder"></span>
                                <span class="catDropTabTitle">所有</span>
                            </li>
                            <li class="catDropTab artist">
                                <span class="catDropTabTopBorder"></span>
                                <span class="catDropTabTitle">艺人</span>
                            </li>
                            <li class="catDropTab venue">
                                <span class="catDropTabTopBorder"></span>
                                <span class="catDropTabTitle">场馆</span>
                            </li>
                            <li class="catDropTab performance">
                                <span class="catDropTabTopBorder"></span>
                                <span class="catDropTabTitle">表演</span>
                            </li>
                        </ul>
                    </div>
                    <div class="catDropList">
                        <ul>
                            <li class="catDropListItem artist">陈奕迅</li>
                            <li class="catDropListItem performance">演唱会</li>
                            <li class="catDropListItem artist">Bruno Mars</li>
                            <li class="catDropListItem venue">上体馆</li>
                            <li class="catDropListItem venue">梅赛德斯·奔驰馆</li>
                            <li class="catDropListItem artist">周杰伦</li>
                            <li class="catDropListItem performance">话剧</li>
                            <li class="catDropListItem venue">虹口足球场</li>
                            <li class="catDropListItem artist">OneRepublic</li>
                            <li class="catDropListItem artist">五月天</li>
                            <li class="catDropListItem performance">马戏团</li>
                            <li class="catDropListItem artist">陈绮贞</li>
                            <li class="catDropListItem venue">上海大舞台</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="pageCenter">
                <div id="logo">
                    <a href="index.php"><h1><img src='img/logo.jpg' class="logo"></h1></a>
                </div>
                <div id="location" class="">
                    <div class="locationChosen">
                        <div class="locChosenTitle">上海</div>
                        <div class="locChosenId">location25</div>
                    </div>
                    <div class="locationIcon"><span></span></div>
                    <div class="locationDropdown">
                        <div class="locDropBlock">
                            <div class="locDropLeft">华北地区</div>
                            <div class="locDropRight">
                                <ul>
                                    <li class="locDropItem" id="location11"><span>北京</span></li>
                                    <li class="locDropItem" id="location12"><span>天津</span></li>
                                    <li class="locDropItem" id="location13"><span>沈阳</span></li>
                                    <li class="locDropItem" id="location14"><span>长春</span></li>
                                    <li class="locDropItem" id="location15"><span>哈尔滨</span></li>
                                    <li class="locDropItem" id="location16"><span>大连</span></li>
                                    <li class="locDropItem" id="location17"><span>太原</span></li>
                                    <li class="locDropItem" id="location18"><span>石家庄</span></li>
                                    <li class="locDropItem" id="location19"><span>郑州</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="locDropBlock">
                            <div class="locDropLeft">华东地区</div>
                            <div class="locDropRight">
                                <ul>
                                    <li class="locDropItem" id="location21"><span>北京</span></li>
                                    <li class="locDropItem" id="location22"><span>天津</span></li>
                                    <li class="locDropItem" id="location23"><span>沈阳</span></li>
                                    <li class="locDropItem" id="location24"><span>长春</span></li>
                                    <li class="locDropItem active" id="location25"><span>上海</span></li>
                                    <li class="locDropItem" id="location26"><span>大连</span></li>
                                    <li class="locDropItem" id="location27"><span>太原</span></li>
                                    <li class="locDropItem" id="location28"><span>石家庄</span></li>
                                    <li class="locDropItem" id="location29"><span>郑州</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="locDropBlock">
                            <div class="locDropLeft">华南地区</div>
                            <div class="locDropRight">
                                <ul>
                                    <li class="locDropItem" id="location31"><span>北京</span></li>
                                    <li class="locDropItem" id="location32"><span>天津</span></li>
                                    <li class="locDropItem" id="location33"><span>沈阳</span></li>
                                    <li class="locDropItem" id="location34"><span>长春</span></li>
                                    <li class="locDropItem" id="location35"><span>哈尔滨</span></li>
                                    <li class="locDropItem" id="location36"><span>大连</span></li>
                                    <li class="locDropItem" id="location37"><span>太原</span></li>
                                    <li class="locDropItem" id="location38"><span>石家庄</span></li>
                                    <li class="locDropItem" id="location39"><span>郑州</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="locDropBlock">
                            <div class="locDropLeft">西部地区</div>
                            <div class="locDropRight">
                                <ul>
                                    <li class="locDropItem" id="location41"><span>北京</span></li>
                                    <li class="locDropItem" id="location42"><span>天津</span></li>
                                    <li class="locDropItem" id="location43"><span>沈阳</span></li>
                                    <li class="locDropItem" id="location44"><span>长春</span></li>
                                    <li class="locDropItem" id="location45"><span>哈尔滨</span></li>
                                    <li class="locDropItem" id="location46"><span>大连</span></li>
                                    <li class="locDropItem" id="location47"><span>太原</span></li>
                                    <li class="locDropItem" id="location48"><span>石家庄</span></li>
                                    <li class="locDropItem" id="location49"><span>郑州</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="search">
                    <form action="search.php" method="GET">
                        <input type="text" name="key" class="placeholder" value="请输入演出、艺人、场馆名称…">
                        <input type="submit" value="">
                        <span class="submitBg"></span>
                    </form>
                </div>
            </div>
            <div id="account">

                <div class="accountHover">
                    <div class="accountIcon"></div>
                    <div class="accountTitle">我的帐户</div>
                </div>
                <div class="accountDropdown">
                    

                    <?php if(!isset($_GET['user'])): ?>

                    <div class="accDropLogin">
                        <div class="accLoginTitle">请登录</div>
                        <form action="" method="POST" class="accLoginForm">
                            <input type="text" class="inputText placeholder" name="username" value="名字">
                            <input type="text" class="inputText placeholder" name="password" value="密码">
                            <a href="" id="forgotPassword" >找回密码</a>
                            <input type="submit" class="inputButton" value="登录">
                        </form>
                    </div>

                    <div class="accDropPassword">
                        <div class="accLoginTitle">找回密码</div>
                        <form action="" method="POST" class="accLoginForm">
                            <input type="text" class="inputText" name="email" placeholder="电子邮件">
                            <input type="submit" class="inputButton" value="发送">
                        </form>
			<div class="accLoginBack">Come back to login</div>
                    </div>


                    <?php else: ?>

                    <div class="accDropDashboard">
                        <div class="accDashSection avatar">
                            <div class="accAvatar">
                                <div class="accAvatarMask"></div>
                                <img src="img/content/avatar_big.jpg">
                            </div>
                            <div class="accUsername">用户名显示在这里</div>
                        </div>
                        <div class="accDashSection settings">
                            <div class="accSetIcon"></div>
                            <div class="accSetTitle">设置</div>
                        </div>
                        <div class="accDashSection updates">
                            <div class="accUpdateTitle">活动</div>
                            <div class="accUpdateListing">
                                <div class="accUpdateItem artist">
                                    <div class="accUpItemMain">角紧了展，维美工也开再性，内局张来就开也华作院手要到直利外角间。</div>
                                    <div class="accUpItemInfo">角紧了展</div>
                                </div>
                                <div class="accUpdateItem venue">
                                    <div class="accUpItemMain">角紧了展，维美工也开再性，内局张来就开也华作院手要到直利外角间。</div>
                                    <div class="accUpItemInfo">角紧了展</div>
                                </div>
                                <div class="accUpdateItem artist">
                                    <div class="accUpItemMain">角紧了展，维美工也开再性，内局张来就开也华作院手要到直利外角间。</div>
                                    <div class="accUpItemInfo">角紧了展</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="accDropSettings">
                        <div class="accDashSection avatar">
                            <div class="accAvatar">
                                <div class="accAvatarMask"></div>
                                <img src="img/content/avatar_big.jpg">
                            </div>
                            <div class="accUsername">用户名显示在这里</div>
                        </div>
                        <div class="accDashSection settings">
                            <div class="accSetIcon"></div>
                            <div class="accLoginTitle">设置</div>
                            <form action="" method="POST" class="accLoginForm">
                                <input type="text" class="inputText placeholder" name="email" value="电子邮件">
                                <input type="text" class="inputText placeholder" name="new_password" value="新密码">
                                <input type="text" class="inputText placeholder" name="new_password2" value="请再次输入">
                                <input type="submit" class="inputButton" value="提交">
                            </form>
                        </div>
                    </div>


                    <?php endif; ?>
                    

                </div>
            </div>
        </div>
    </div>
