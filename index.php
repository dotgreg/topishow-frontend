<?php
    include('inc/header.php');
?>

<div id="main" class="fullWidth">
  <div class="pageCenter">
    <div class="header_small_trigger"></div>


    <?php include('inc/slide.php'); ?>



    <div class="sectionTitle">
        <div class="sectionTitleBg">
            <div class="titleStars"></div>
            <div class="titleStripe1"></div>
            <div class="titleStripe2"></div>
        </div>
        <h2>排名前十</h2>
    </div>



    <?php

        $contentType           = "artist";
        $contentTypeTitle      = "艺人";
        $contentTypeLink       = "listing.php";
        $contentTypeNo         = 1;

        include('inc/chart.php');


        $contentType           = "performance";
        $contentTypeTitle      = "表演";
        $contentTypeLink       = "listing.php";
        $contentTypeNo         = 2;

        include('inc/chart.php');



        $contentType           = "venue";
        $contentTypeTitle      = "场馆";
        $contentTypeLink       = "listing.php";
        $contentTypeNo         = 3;

        include('inc/chart.php');

    ?>



  </div>
</div>


<?php
    include('inc/footer.php');
?>
