<?php
    include('inc/header.php');
?>

<div id="main" class="fullWidth">
  <div class="pageCenter">
    <div class="header_small_trigger"></div>


        <?php include('inc/slide.php'); ?>



        <div class="section filters">
            <div class="filterLeft">
                <div class="filterLabel">筛选：</div>
                <div class="filterKeywords">
                    <ul>
                        <li class="filterKeyword">
                            <span class="fKeywordTitle">风格1</span>
                            <span class="fKeywordClose"></span>
                        </li>
                        <li class="filterKeyword">
                            <span class="fKeywordTitle">陈奕迅</span>
                            <span class="fKeywordClose"></span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="filterButtons">
                <ul>
                    <li class="filterButton filter">
                        <span class="fButtonTitle">筛选</span>
                        <span class="fButtonIcon"></span>
                        <div class="filterDropdown">
                            <input type="checkbox" class="fDropCheckbox" name="filter" id="filter1">
                            <label for="filter1" class="fDropLabel">艺人</label>                        
                            <input type="checkbox" class="fDropCheckbox" name="filter" id="filter2">
                            <label for="filter2" class="fDropLabel">表演</label>                            
                            <input type="checkbox" class="fDropCheckbox" name="filter" id="filter3">
                            <label for="filter3" class="fDropLabel">场所</label>                            
                        </div>
                    </li>
                    <li class="filterButton reset">
                        <span class="fButtonTitle">重置</span>
                        <span class="fButtonIcon"></span>
                    </li>
                </ul>
            </div>
        </div>



        <div class="section listing artist">

                <div class="listingTitle">
                        <span>125位艺人</span>
                        <span class="listingLine left"></span>
                        <span class="listingLine right"></span>
                </div>


            <div class="listingMain">
                <div class="listingItems">
                    <div class="listingRow listingRow-1 firstRow">
                        <div class="listingItem listingItem-1 first artId32147">
                            <div class="lItemCover">
                                    <a href="artist.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="artist.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                        <div class="listingItem listingItem-2 artId32247">
                            <div class="lItemCover">
                                    <a href="artist.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="artist.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的<br>再加一行描述，因为有的内容会长一些</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                        <div class="listingItem listingItem-3 artId32167">
                            <div class="lItemCover">
                                    <a href="artist.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="artist.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                        <div class="listingItem listingItem-4 last artId32149">
                            <div class="lItemCover">
                                    <a href="artist.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="artist.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                    </div>
                    <div class="listingRow listingRow-2 lastRow">
                        <div class="listingItem listingItem-1 first artId32147">
                            <div class="lItemCover">
                                    <a href="artist.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="artist.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                        <div class="listingItem listingItem-2 artId32247">
                            <div class="lItemCover">
                                    <a href="artist.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="artist.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的<br>再加一行描述，因为有的内容会长一些</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                        <div class="listingItem listingItem-3 artId32167">
                            <div class="lItemCover">
                                    <a href="artist.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="artist.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                        <div class="listingItem listingItem-4 last artId32149">
                            <div class="lItemCover">
                                    <a href="artist.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="artist.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                    </div>
                </div>

                <div class="listingLoader">
                    <div class="lLoaderButton">
                        <span class="lLoaderTitle">查看全部</span>
                        <span class="lLoaderRight"><span class="lLoaderIcon"></span></span>
                    </div>
                </div>

                <!-- <div class="listingPager">
                    <ul class="pager">
                        <li class="pagerItem prev">
                            <a href=""><</a>
                        </li>
                        <li class="pagerItem">
                            <a href="">1</a>
                        </li>
                        <li class="pagerItem">
                            <a href="">2</a>
                        </li>
                        <li class="pagerItem active">3</li>
                        <li class="pagerItem">
                            <a href="">4</a>
                        </li>
                        <li class="pagerItem">
                            <a href="">5</a>
                        </li>
                        <li class="pagerItem next">
                            <a href="">></a>
                        </li>
                    </ul>
                </div> -->
            </div>

        </div>



        <div class="section listing performance">

                <div class="listingTitle">
                        <span>120场表演</span>
                        <span class="listingLine left"></span>
                        <span class="listingLine right"></span>
                </div>


            <div class="listingMain">
                <div class="listingItems">
                    <div class="listingRow listingRow-1 firstRow">
                        <div class="listingItem listingItem-1 first perId32147">
                            <div class="lItemCover">
                                    <a href="performance.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="performance.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                        <div class="listingItem listingItem-2 perId32247">
                            <div class="lItemCover">
                                    <a href="performance.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="performance.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的<br>再加一行描述，因为有的内容会长一些</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                        <div class="listingItem listingItem-3 perId32167">
                            <div class="lItemCover">
                                    <a href="performance.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="performance.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                        <div class="listingItem listingItem-4 last perId32149">
                            <div class="lItemCover">
                                    <a href="performance.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="performance.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                    </div>
                    <div class="listingRow listingRow-2 lastRow">
                        <div class="listingItem listingItem-1 first perId32147">
                            <div class="lItemCover">
                                    <a href="performance.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="performance.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                        <div class="listingItem listingItem-2 perId32247">
                            <div class="lItemCover">
                                    <a href="performance.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="performance.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的<br>再加一行描述，因为有的内容会长一些</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                        <div class="listingItem listingItem-3 perId32167">
                            <div class="lItemCover">
                                    <a href="performance.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="performance.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的</div>
                            </div> 
                            <div class="lItemRating">
                                <div class="rStarGroup">
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarFull"></span>
                                    <span class="rStar rStarHalf"></span>
                                    <span class="rStar rStarEmpty"></span>
                                    <input type="hidden" name="rating" value="3.5" >
                                </div> 
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                        <div class="listingItem listingItem-4 last perId32149">
                            <div class="lItemCover">
                                    <a href="performance.php"><img src="img/content/listing.jpg"></a>
                            </div>
                            <div class="lItemTitle">
                                    <a href="performance.php">陈奕迅</a>
                              </div>
                            <div class="lItemDecription">
                                <div class="lItemDescRow">描述的文字</div>
                                <div class="lItemDescRow">或者标签之类的</div>
                            </div> 
                            <div class="lItemRating">
                                <?php include('inc/rating.php'); ?>
                            </div> 
                            <div class="lItemBottomBorder"></div>
                        </div>
                    </div>
                </div>

                <div class="listingLoader">
                    <div class="lLoaderButton">
                        <span class="lLoaderTitle">查看全部</span>
                        <span class="lLoaderRight"><span class="lLoaderIcon"></span></span>
                    </div>
                </div>
            </div>

        </div>



        <div class="section listing venue">

                <div class="listingTitle">
                        <span>0个场馆</span>
                        <span class="listingLine left"></span>
                        <span class="listingLine right"></span>
                </div>

        </div>





  </div>
</div>


<?php
    include('inc/footer.php');
?>
