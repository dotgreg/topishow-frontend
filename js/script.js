
$.fn.csscroll = function(){    
        var anim = new Array();  
        anim['props'] = arguments[0];      
        anim['vals'] = arguments[1];    
        anim['offset'] = arguments[2];    
        anim['speed'] = arguments[3];    
  
        anim['propsCount'] = arguments[0].length;  
    
        oldvals = new Array();  
        for(i=0;i<anim['propsCount'];i++)  
        {  
            oldvals[i] = $(this).css(anim['props'][i]);  
        }  
        anim['oldvals'] = oldvals;   
  
        console.log('anim vars:');  
        console.log(anim);  
  
        var thism = $(this);  
  
        //if speed is not defined  
        if(typeof anim['speed'] == 'undefined'){anim['speed'] = 500};     
  
        function animateScroll(anim){  
            windowTop = $(window).scrollTop();  
            if(windowTop > anim['offset'])  
            {  
                var aniArgs = {};  
                for(i=0;i<anim['propsCount'];i++)  
                {  
                    aniArgs[anim['props'][i]] = anim['vals'][i];  
                }  
  
                console.log('aniArgs new:');  
                console.log(aniArgs);  
  
                thism.stop(false,false).animate(aniArgs,anim['speed']);  
            }  
            else  
            {  
               var aniArgs = {};  
                for(i=0;i<anim['propsCount'];i++)  
                {  
                    aniArgs[anim['props'][i]] = anim['oldvals'][i];  
                }  
                thism.stop(false,false).animate(aniArgs,anim['speed']);  
            }  
        }  
  
    animateScroll(anim)  
    $(window).scroll(function(){  
        animateScroll(anim)  
    });  
};    

$(function(){
    
    
    
    $(document).ready(function(w) {  
        $(window).load(function(w) {  
	    $('.header_small_trigger').waypoint(function(){
		$('#header').addClass('small');
	    });
	    $('.header_small_trigger').waypoint(function(){
		$('#header, .section.banner').removeClass('small');
	    },{offset:'10%'});
        });
    });

    // Header scroll function
    $(window).scroll(function(){

    })

	// search box placeholder

	var placeholder = $('#search input[type=text]').val();

	$('#search input[type=text]').focus(function(){

		if($(this).hasClass("placeholder")){
			$(this).removeClass("placeholder");
			$(this).val("");
		};

	});

	$('#search input[type=text]').blur(function(){

		var value = $(this).val();

		if(value == placeholder){
			$(this).addClass("placeholder");}
		else if(value == ""){
			$(this).val(placeholder);
			$(this).addClass("placeholder");
		};
	});




	// category dropdown


	$('.catDropTab').click(function(){

		if(!$(this).hasClass('active')){

			$('.catDropTab').removeClass('active');
			$(this).addClass('active');

			var cat = 'all';

			if($(this).hasClass('artist')){
				cat = 'artist';
			}else if($(this).hasClass('performance')){
				cat = 'performance';
			}else if($(this).hasClass('venue')){
				cat = 'venue';
			};

			$('.catDropList').removeClass('artist performance venue all').addClass(cat);

		}


	});




	// location dropdown


	$('.locDropItem').click(function(){

		if(!$(this).hasClass('active')){

			var locId, locTitle;

			locId = $(this).attr('id');
			locTitle = $(this).text();

			$('.locChosenTitle').text(locTitle);
			$('.locChosenId').text(locId);

			$('#location').removeClass('active');

			$('.locDropItem').removeClass('active');
			$(this).addClass('active');

		};

	});




	// account dropdown

	$('.accountHover').click(function(){


		if($('#account').hasClass('active')){

			$('#account').removeClass('active');
			$('.accDropLogin').removeClass('inactive');
			$('.accDropPassword').removeClass('active');
			$('.accDropDashboard').removeClass('inactive');
			$('.accDropSettings').removeClass('active');

		}else{

		}


	});


	$('#forgotPassword').click(function(e){

	    e.preventDefault();
	    $('.accDropLogin').removeClass('active').addClass('inactive');
	    $('.accDropPassword').removeClass('inactive').addClass('active');
	    $('.accLoginBack').click(function(e){
		$('.accDropLogin').removeClass('inactive').addClass('active');
		$('.accDropPassword').removeClass('active').addClass('inactive');
	    });
	})


	$('.accDropDashboard .accDashSection.settings').click(function(){

		$('.accDropDashboard').removeClass('active').addClass('inactive');
		$('.accDropSettings').removeClass('inactive').addClass('active');

	})

    $('.accDashSection.avatar').click(function(){
	$('.accDropDashboard').removeClass('inactive').addClass('active');
	$('.accDropSettings').removeClass('active').addClass('inactive');
    });


    $('.mCCmtReply').click(function(){
	$(".mCCmtReplyBox").removeClass("active");
	$(this).parent().find(".mCCmtReplyBox").addClass("active");
    });

	// placeholders for user forms

	var ph_log_username, ph_log_password, ph_email, ph_password1, ph_password2;

	ph_log_username = $('.accLoginForm input[name=username]').val();
	ph_log_password = $('.accLoginForm input[name=password]').val();
	ph_email = $('.accLoginForm input[name=email]').val();
	ph_password1 = $('.accLoginForm input[name=new_password]').val();
	ph_password2 = $('.accLoginForm input[name=new_password2]').val();

	$('.accLoginForm input[type=text], .accLoginForm input[type=password]').focus(function(){

		if($(this).hasClass("placeholder")){
			$(this).removeClass("placeholder");
			$(this).val("");
			if($(this).attr('name') == 'password' || $(this).attr('name') == 'new_password' || $(this).attr('name') == 'new_password2'){
				$(this).attr('type','password');
			}
		};

	});

	$('.accLoginForm input[type=text], .accLoginForm input[type=password]').blur(function(){

		var value, placeholder;

		value = $(this).val();

		if(value == ""){

			if($(this).attr('name') == 'username'){
				placeholder = ph_log_username;
			}else if($(this).attr('name') == 'password'){
				placeholder = ph_log_password;
				$(this).attr('type','text');
			}else if($(this).attr('name') == 'email'){
				placeholder = ph_email;
			}else if($(this).attr('name') == 'new_password'){
				placeholder = ph_password1;
				$(this).attr('type','text');
			}else if($(this).attr('name') == 'new_password2'){
				placeholder = ph_password2;
				$(this).attr('type','text');
			}

			$(this).val(placeholder);
			$(this).addClass("placeholder");

		};
	});




	// slideshow

	if($('.section.slideshow').length > 0){


		// bullet positioning on the slideshow

		var slide_width, bullet_width, title_width, bullet_counter, bullet_id_active, bullet_id, bullet_offset, title_active;

		slide_width 			= 1020;
		bullet_width 			= 60;
		bullet_id_active 	= 1;
		bullet_counter 		= $('.section.slideshow .bullets .bullet').length;
		title_width				= slide_width - bullet_width * bullet_counter;

		$('.section.slideshow .slideDesc').css('width', title_width + 'px');

		bullet_id_active = $('.section.slideshow .bullets .bullet.active').attr('id');
		bullet_id_active = parseInt(bullet_id_active.substring(8));

		for( i=1 ; i<=bullet_counter ; i++ ){

			if(i <= bullet_id_active){
				bullet_offset = (i - 1) * bullet_width;
			}else{
				bullet_offset = (i - 1) * bullet_width + title_width;
			}

			bullet_id = "#sBullet-" + i;
			$('.section.slideshow '+bullet_id).css('left', bullet_offset+'px');

		}


		title_active = $('.section.slideshow .bullet.active span.hidden').text();
		$('.section.slideshow .slideDesc').css('left', bullet_id_active * bullet_width + 'px').text(title_active);


		$('#slideActive').val(bullet_id_active);
		$('#slideTotal').val(bullet_counter);


		// bullet action

		$('.section.slideshow .bullets .bullet').hover(function(){

			if(!$(this).hasClass('active')){

				var tgt_id, active_id, tgt_slide, tgt_title, content_type;


				active_id 	= parseInt($('#slideActive').val());
				tgt_id 			= $(this).attr('id');
				tgt_id 			= parseInt(tgt_id.substring(8));
				tgt_slide 	= "#sImage-"+tgt_id;
				tgt_title 	= $(this).find('span.hidden').text();

				content_type  = "artist";
				if($(this).hasClass("venue")){
					content_type  = "venue";
				}else if($(this).hasClass("performance")){
					content_type  = "performance";
				}

				$('.section.slideshow .bullet.active, .section.slideshow .slideImage.active').removeClass('active');
				$(this).addClass('active');
				$(tgt_slide).addClass('active');
				$('#slideActive').val(tgt_id);


				$('.section.slideshow .slideDesc').fadeOut(100,function(){

					$('.section.slideshow .slideDesc').text("");
					$('.section.slideshow').removeClass('artist venue performance').addClass(content_type);

					for( j=1 ; j<=tgt_id ; j++ ){
						bullet_offset = (j - 1) * bullet_width;
						bullet_id = "#sBullet-" + j;
						$('.section.slideshow '+bullet_id).animate({
								'left':bullet_offset+'px'
							},{
						    duration: 200,
						    specialEasing: "easeOutBounce"
						});
					};

					for( k=tgt_id + 1 ; k<=bullet_counter ; k++){
						bullet_offset = (k - 1) * bullet_width + title_width;
						bullet_id = "#sBullet-" + k;
						$('.section.slideshow '+bullet_id).animate({
								'left':bullet_offset+'px'
							},{
						    duration: 200,
						    specialEasing: "easeOutBounce"
						});
					};

					$('.section.slideshow .slideDesc').text(tgt_title).css('left', tgt_id * bullet_width + 'px').fadeIn(100);

				})



			}

		});

	}




	// homepage charts

	$('.chartDisplay').click(function(){

		var trigger_id, trigger_id_str, type_no, filter_no, item_no, menu_item_id, display_id, menu_id;

		trigger_id 	= $(this).attr('id');
		trigger_id_str	= trigger_id.split('-');

		type_no			= trigger_id_str[0].substring(4);
		filter_no		= trigger_id_str[1].substring(4);
		item_no 		= trigger_id_str[2];

		display_id  = "#type"+type_no+"-disp"+filter_no;
		menu_id  		= "#type"+type_no+"-menu"+filter_no;
		menu_item_id 		= "#type"+type_no+"-menu"+filter_no+"-"+item_no;


		if(!$(this).hasClass('selected')){

			$(display_id).find('.chartDisplay').removeClass('selected');
			$(this).addClass('selected');

			$(menu_id).find('.chartItem').removeClass('selected');
			$(menu_item_id).addClass('selected');

		}else{

			$(display_id).find('.chartDisplay').removeClass('selected');
			$(menu_id).find('.chartItem').removeClass('selected');

		}

	});


	$('.cFilter').click(function(){

		if(!$(this).hasClass('active')){

			var filter_id, filter_id_str, type_no, filter_no, menu_id, display_id;

			filter_id 			= $(this).attr('id');
			filter_id_str 	= filter_id.split('-');

			type_no					= filter_id_str[0].substring(4);
			filter_no				= filter_id_str[1].substring(4);

			menu_id 				= "#type" + type_no + "-menu" + filter_no;
			display_id 			= "#type" + type_no + "-disp" + filter_no;

			$(this).closest('.chartFilters').find('.cFilter').removeClass('active');
			$(this).addClass('active');

			$(this).closest('.section.charts').find('.chartItems ul').removeClass('active');
			$(menu_id).addClass('active');

			$(this).closest('.section.charts').find('.chartDisplays').removeClass('active');
			$(display_id).addClass('active');

			$(this).closest('.section.charts').find('.chartItem, .chartDisplay').removeClass('selected');

		}

	});


	$('.chartItem').hover(function(){

		var trigger_id, trigger_id_str, type_no, filter_no, item_no, display_item_id;

		trigger_id 			= $(this).attr('id');
		trigger_id_str	= trigger_id.split('-');

		type_no					= trigger_id_str[0].substring(4);
		filter_no				= trigger_id_str[1].substring(4);
		item_no 				= trigger_id_str[2];

		display_item_id 		= "#type"+type_no+"-disp"+filter_no+"-"+item_no;

		if(!$(this).hasClass('selected')){

			$(display_item_id).addClass('selected');

		}

	},function(){

		var trigger_id, trigger_id_str, type_no, filter_no, item_no, display_item_id;

		trigger_id 			= $(this).attr('id');
		trigger_id_str	= trigger_id.split('-');

		type_no					= trigger_id_str[0].substring(4);
		filter_no				= trigger_id_str[1].substring(4);
		item_no 				= trigger_id_str[2];

		display_item_id 		= "#type"+type_no+"-disp"+filter_no+"-"+item_no;

		if(!$(this).hasClass('selected')){

			$(display_item_id).removeClass('selected');

		}

	});


	$('.chartItem').click(function(){

		var trigger_id, trigger_id_str, type_no, filter_no, item_no, menu_id, display_id, display_item_id;

		trigger_id 			= $(this).attr('id');
		trigger_id_str	= trigger_id.split('-');

		type_no					= trigger_id_str[0].substring(4);
		filter_no				= trigger_id_str[1].substring(4);
		item_no 				= trigger_id_str[2];

		menu_id  						= "#type"+type_no+"-menu"+filter_no;
		display_id  				= "#type"+type_no+"-disp"+filter_no;
		display_item_id 		= "#type"+type_no+"-disp"+filter_no+"-"+item_no;

		if(!$(this).hasClass('selected')){

			$(menu_id).find('.chartItem').removeClass('selected');
			$(this).addClass('selected');

			$(display_id).find('.chartDisplay').removeClass('selected');
			$(display_item_id).addClass('selected');

		}else{

			$(menu_id).find('.chartItem').removeClass('selected');
			$(display_id).find('.chartDisplay').removeClass('selected');

		}

	});





	// listing filter tab

	$('.filterButton.filter').click(function(){

		$(this).toggleClass('active');

	});

	$('.filterDropdown input[type=checkbox]').change(function(){

		var title, active, filter, trigger_id, filter_id;

		trigger_id 	= $(this).attr('id');
		filter_id 	= "list_filter_"+trigger_id;
		title 			= $(this).next('label').text();
		active 			= $(this).is(':checked');

		if(active){

			filter  = "<li class=filterKeyword id="+filter_id+">";
			filter += "<span class=fKeywordTitle>"+title+"</span>";
			filter += "<span class=fKeywordClose></span>";
			filter += "</li>";

			$('.filterKeywords ul').append(filter);

			filterKeywordClose(filter_id);

		}else{
			
			$('.filterKeywords').find('#'+filter_id).remove();

		}

	});


	filterKeywordClose();





	// main content tab

	$('.mTabArea .mTab').click(function(){

		if(!$(this).hasClass('active')){

			$('.mTabArea .mTab').removeClass('active');
			$(this).addClass('active');

			var tab_no, card_id;

			tab_no = $(this).attr('id').substring(5);
			card_id = "#mCard-"+tab_no;

			$('.mCardArea .mCard').removeClass('active');
			$(card_id).addClass('active');

		}

	});




	$('.mCAlbum').each(function(){

		var album_id, album_class, content_type;

		album_id = $(this).attr('id').substring(5);
		album_class = "mCAlbumPhoto-" + album_id;

		content_type = "artist";
		if($('#main').hasClass('venue')){
			content_type = "venue";
		}else if($('#main').hasClass('performance')){
			content_type = "performance";
		}

		$('.'+album_class).colorbox({
			rel: album_class,
			className: content_type
		});

	})



	// comment placeholder

	var cm_placeholder = $('.mCCmtForm textarea').val();

	$('.mCCmtForm textarea').focus(function(){

		if($(this).hasClass("placeholder")){
			$(this).removeClass("placeholder");
			$(this).val("");
		};

	});

	$('.mCCmtForm textarea').blur(function(){

		var value = $(this).val();

		if(value == cm_placeholder){
			$(this).addClass("placeholder");}
		else if(value == ""){
			$(this).val(cm_placeholder);
			$(this).addClass("placeholder");
		};
	});



	$('.footerToTop').click(function(){

		$('html, body').animate({
			scrollTop : 0
		},{
			duration: 500,
			specialEasing: "easeOutBounce"
		});

	});



});







// Bind the close function to filters on the listing page

function filterKeywordClose(id){

	var id_str = "";

	if(typeof(id) != "undefined" && id !== null){
		id_str = "#"+id+" ";
	}

	$(id_str+'.fKeywordClose').click(function(){

		var filter_id, source_id;

		filter_id = $(this).closest('.filterKeyword').attr('id');

		if(typeof(filter_id) != "undefined" && filter_id !== null){
			if(filter_id.indexOf('list_filter_') >= 0){
				source_id = filter_id.substring(12);
				$('.filterDropdown #'+source_id).attr('checked',false);
			}
		}

		$(this).closest('.filterKeyword').remove();

	})

}
